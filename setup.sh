cp lib64/libvppinfra.so.21.01.1 /usr/lib64/
cp lib64/libvnet.so.21.01.1 /usr/lib64/
cp lib64/libvlibmemory.so.21.01.1 /usr/lib64/
cp lib64/libvlib.so.21.01.1 /usr/lib64/
cp lib64/libsvm.so.21.01.1 /usr/lib64/
cp lib64/libnat.so.21.01.1 /usr/lib64/

mkdir /usr/lib64/vpp_plugins

cp -rf lib64/vpp_plugins/dpdk_plugin.so /usr/lib64/vpp_plugins/
cp -rf lib64/vpp_plugins/upf_plugin.so /usr/lib64/vpp_plugins/

wget http://repo.openfusion.net/centos7-x86_64/hyperscan-devel-5.3.0-1.of.el7.x86_64.rpm \
         http://repo.openfusion.net/centos7-x86_64/hyperscan-5.3.0-1.of.el7.x86_64.rpm && \
    rpm -i *.rpm && rm *.rpm

groupadd vpp
